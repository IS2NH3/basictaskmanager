﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace basictask26
{
    public partial class BasicTaskManager : Form
    {
        public BasicTaskManager()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            {
                if (textBox_id.Text == "" || textBox_nom.Text == "" || textBox_int.Text == "" || textBox_date.Text == "") //si au moins un champs est vide, alors il faut compléter
                {
                    MessageBox.Show(" Veuillez remplir tous les champs ");
                }
                else
                {
                    try
                    {

                        this.dataGridView1.Rows.Add(textBox_id.Text, textBox_nom.Text, textBox_int.Text, textBox_date.Text); // ajouts des informations entrées
                        this.textBox_id.Text = "";
                        this.textBox_nom.Clear();
                        this.textBox_int.Clear();
                        this.textBox_date.Clear();
                    }

                    catch (FormatException)
                    {
                        MessageBox.Show("");
                    }
                }
            }
        }

        private void BasicTaskManager_Load(object sender, EventArgs e)
        {
            this.groupBox1.Visible = false;
        }

        private void btn_supprimer_Click(object sender, EventArgs e)
        {
            {
                try
                {
                    DialogResult res = MessageBox.Show("Veuillez confirmez la suppression ?", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    if (res == DialogResult.OK)
                    {
                        int index = this.dataGridView1.CurrentRow.Index;
                        this.dataGridView1.Rows.RemoveAt(index);
                        MessageBox.Show("Suppression réalisée");
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show(" Veuillez sélectionner une ligne du tableau ");
                }
            }
        }
        int p;
        private void btn_modifier_Click(object sender, EventArgs e)
        {
            int k = 0;

            for (int i = 0; i < this.dataGridView1.Rows.Count - 1; i++)
            {
                if (this.dataGridView1.Rows[i].Cells[0].Value.ToString() == textBox_modifier.Text)
                {
                    p = i;
                    k = 1;
                }
            }
            if (k == 0)
                MessageBox.Show(" L'id n'est pas valide ");
            else
                this.groupBox1.Visible = true;
        }






        private void button1_Click_1(object sender, EventArgs e) // Permet d'appliquer les modifications
        {
            this.dataGridView1.Rows[p].Cells[0].Value = textBox_id2.Text;
            this.dataGridView1.Rows[p].Cells[1].Value = textBox_nom2.Text;
            this.dataGridView1.Rows[p].Cells[2].Value = textBox_int2.Text;
            this.dataGridView1.Rows[p].Cells[3].Value = textBox_deadline2.Text;
            this.textBox_id2.Clear();
            this.textBox_nom2.Clear();
            this.textBox_int2.Clear();
            this.textBox_deadline2.Clear();
            this.textBox_modifier.Clear();
            this.groupBox1.Visible = false;
        }

        private void quitter_Click(object sender, EventArgs e) // Permet de quitter l'application 
        {
            DialogResult dialog = MessageBox.Show(" Voulez-vous vraiment quitter la meilleure appli au monde  ?", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (dialog == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void textBox_id2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox_nom2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox_int2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox_deadline2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox_modifier_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void title_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }

        private void label3_Click_1(object sender, EventArgs e)
        {

        }

        private void label_deadline2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label_modifier_Click(object sender, EventArgs e)
        {

        }

        private void btn_trier_Click(object sender, EventArgs e)
        {
        //    dataGridView1.Sort(dataGridView1.col_intitule, System.ComponentModel.ListSortDirection.Ascending);
        }
    }
}
