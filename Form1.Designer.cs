﻿namespace basictask26
{
    partial class BasicTaskManager
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_ajouter = new System.Windows.Forms.Button();
            this.btn_modifier = new System.Windows.Forms.Button();
            this.btn_supprimer = new System.Windows.Forms.Button();
            this.textBox_id = new System.Windows.Forms.TextBox();
            this.textBox_nom = new System.Windows.Forms.TextBox();
            this.label_id = new System.Windows.Forms.Label();
            this.label_nom = new System.Windows.Forms.Label();
            this.label_int = new System.Windows.Forms.Label();
            this.label_deadline = new System.Windows.Forms.Label();
            this.textBox_int = new System.Windows.Forms.TextBox();
            this.textBox_date = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_ok = new System.Windows.Forms.Button();
            this.textBox_deadline2 = new System.Windows.Forms.TextBox();
            this.textBox_int2 = new System.Windows.Forms.TextBox();
            this.textBox_nom2 = new System.Windows.Forms.TextBox();
            this.textBox_id2 = new System.Windows.Forms.TextBox();
            this.label_deadline2 = new System.Windows.Forms.Label();
            this.label_int2 = new System.Windows.Forms.Label();
            this.label_nom2 = new System.Windows.Forms.Label();
            this.label_id2 = new System.Windows.Forms.Label();
            this.label_modifier = new System.Windows.Forms.Label();
            this.textBox_modifier = new System.Windows.Forms.TextBox();
            this.quitter = new System.Windows.Forms.Button();
            this.title = new System.Windows.Forms.Label();
            this.btn_trier = new System.Windows.Forms.Button();
            this.col_nom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_intitule = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_dead = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_nom,
            this.col_id,
            this.col_intitule,
            this.col_dead});
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(443, 230);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // btn_ajouter
            // 
            this.btn_ajouter.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_ajouter.Location = new System.Drawing.Point(228, 320);
            this.btn_ajouter.Name = "btn_ajouter";
            this.btn_ajouter.Size = new System.Drawing.Size(75, 73);
            this.btn_ajouter.TabIndex = 1;
            this.btn_ajouter.Text = "Ajouter";
            this.btn_ajouter.UseVisualStyleBackColor = true;
            this.btn_ajouter.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_modifier
            // 
            this.btn_modifier.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_modifier.Location = new System.Drawing.Point(740, 401);
            this.btn_modifier.Name = "btn_modifier";
            this.btn_modifier.Size = new System.Drawing.Size(75, 73);
            this.btn_modifier.TabIndex = 2;
            this.btn_modifier.Text = "Modifier";
            this.btn_modifier.UseVisualStyleBackColor = true;
            this.btn_modifier.Click += new System.EventHandler(this.btn_modifier_Click);
            // 
            // btn_supprimer
            // 
            this.btn_supprimer.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_supprimer.Location = new System.Drawing.Point(228, 401);
            this.btn_supprimer.Name = "btn_supprimer";
            this.btn_supprimer.Size = new System.Drawing.Size(75, 73);
            this.btn_supprimer.TabIndex = 3;
            this.btn_supprimer.Text = "Supprimer";
            this.btn_supprimer.UseVisualStyleBackColor = true;
            this.btn_supprimer.Click += new System.EventHandler(this.btn_supprimer_Click);
            // 
            // textBox_id
            // 
            this.textBox_id.Location = new System.Drawing.Point(73, 321);
            this.textBox_id.Name = "textBox_id";
            this.textBox_id.Size = new System.Drawing.Size(100, 20);
            this.textBox_id.TabIndex = 4;
            this.textBox_id.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox_nom
            // 
            this.textBox_nom.Location = new System.Drawing.Point(73, 358);
            this.textBox_nom.Name = "textBox_nom";
            this.textBox_nom.Size = new System.Drawing.Size(100, 20);
            this.textBox_nom.TabIndex = 5;
            // 
            // label_id
            // 
            this.label_id.AutoSize = true;
            this.label_id.Location = new System.Drawing.Point(20, 327);
            this.label_id.Name = "label_id";
            this.label_id.Size = new System.Drawing.Size(15, 14);
            this.label_id.TabIndex = 6;
            this.label_id.Text = "id";
            this.label_id.Click += new System.EventHandler(this.label1_Click);
            // 
            // label_nom
            // 
            this.label_nom.AutoSize = true;
            this.label_nom.Location = new System.Drawing.Point(20, 364);
            this.label_nom.Name = "label_nom";
            this.label_nom.Size = new System.Drawing.Size(28, 14);
            this.label_nom.TabIndex = 7;
            this.label_nom.Text = "nom";
            this.label_nom.Click += new System.EventHandler(this.label2_Click);
            // 
            // label_int
            // 
            this.label_int.AutoSize = true;
            this.label_int.Location = new System.Drawing.Point(20, 401);
            this.label_int.Name = "label_int";
            this.label_int.Size = new System.Drawing.Size(40, 14);
            this.label_int.TabIndex = 8;
            this.label_int.Text = "intitulé";
            this.label_int.Click += new System.EventHandler(this.label3_Click);
            // 
            // label_deadline
            // 
            this.label_deadline.AutoSize = true;
            this.label_deadline.Location = new System.Drawing.Point(20, 442);
            this.label_deadline.Name = "label_deadline";
            this.label_deadline.Size = new System.Drawing.Size(47, 14);
            this.label_deadline.TabIndex = 10;
            this.label_deadline.Text = "Deadline";
            // 
            // textBox_int
            // 
            this.textBox_int.Location = new System.Drawing.Point(73, 395);
            this.textBox_int.Name = "textBox_int";
            this.textBox_int.Size = new System.Drawing.Size(100, 20);
            this.textBox_int.TabIndex = 11;
            // 
            // textBox_date
            // 
            this.textBox_date.Location = new System.Drawing.Point(73, 436);
            this.textBox_date.Name = "textBox_date";
            this.textBox_date.Size = new System.Drawing.Size(100, 20);
            this.textBox_date.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_ok);
            this.groupBox1.Controls.Add(this.textBox_deadline2);
            this.groupBox1.Controls.Add(this.textBox_int2);
            this.groupBox1.Controls.Add(this.textBox_nom2);
            this.groupBox1.Controls.Add(this.textBox_id2);
            this.groupBox1.Controls.Add(this.label_deadline2);
            this.groupBox1.Controls.Add(this.label_int2);
            this.groupBox1.Controls.Add(this.label_nom2);
            this.groupBox1.Controls.Add(this.label_id2);
            this.groupBox1.Font = new System.Drawing.Font("Vijaya", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(503, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(312, 185);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saisissez vos nouvelles données";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // btn_ok
            // 
            this.btn_ok.Location = new System.Drawing.Point(231, 69);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(75, 73);
            this.btn_ok.TabIndex = 8;
            this.btn_ok.Text = "OK";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // textBox_deadline2
            // 
            this.textBox_deadline2.Location = new System.Drawing.Point(125, 142);
            this.textBox_deadline2.Name = "textBox_deadline2";
            this.textBox_deadline2.Size = new System.Drawing.Size(100, 24);
            this.textBox_deadline2.TabIndex = 7;
            this.textBox_deadline2.TextChanged += new System.EventHandler(this.textBox_deadline2_TextChanged);
            // 
            // textBox_int2
            // 
            this.textBox_int2.Location = new System.Drawing.Point(125, 103);
            this.textBox_int2.Name = "textBox_int2";
            this.textBox_int2.Size = new System.Drawing.Size(100, 24);
            this.textBox_int2.TabIndex = 6;
            this.textBox_int2.TextChanged += new System.EventHandler(this.textBox_int2_TextChanged);
            // 
            // textBox_nom2
            // 
            this.textBox_nom2.Location = new System.Drawing.Point(125, 69);
            this.textBox_nom2.Name = "textBox_nom2";
            this.textBox_nom2.Size = new System.Drawing.Size(100, 24);
            this.textBox_nom2.TabIndex = 5;
            this.textBox_nom2.TextChanged += new System.EventHandler(this.textBox_nom2_TextChanged);
            // 
            // textBox_id2
            // 
            this.textBox_id2.Location = new System.Drawing.Point(125, 31);
            this.textBox_id2.Name = "textBox_id2";
            this.textBox_id2.Size = new System.Drawing.Size(100, 24);
            this.textBox_id2.TabIndex = 4;
            this.textBox_id2.TextChanged += new System.EventHandler(this.textBox_id2_TextChanged);
            // 
            // label_deadline2
            // 
            this.label_deadline2.AutoSize = true;
            this.label_deadline2.Location = new System.Drawing.Point(64, 148);
            this.label_deadline2.Name = "label_deadline2";
            this.label_deadline2.Size = new System.Drawing.Size(58, 22);
            this.label_deadline2.TabIndex = 3;
            this.label_deadline2.Text = "Deadline";
            this.label_deadline2.Click += new System.EventHandler(this.label_deadline2_Click);
            // 
            // label_int2
            // 
            this.label_int2.AutoSize = true;
            this.label_int2.Location = new System.Drawing.Point(64, 109);
            this.label_int2.Name = "label_int2";
            this.label_int2.Size = new System.Drawing.Size(50, 22);
            this.label_int2.TabIndex = 2;
            this.label_int2.Text = "intitulé";
            this.label_int2.Click += new System.EventHandler(this.label3_Click_1);
            // 
            // label_nom2
            // 
            this.label_nom2.AutoSize = true;
            this.label_nom2.Location = new System.Drawing.Point(64, 69);
            this.label_nom2.Name = "label_nom2";
            this.label_nom2.Size = new System.Drawing.Size(33, 22);
            this.label_nom2.TabIndex = 1;
            this.label_nom2.Text = "nom";
            this.label_nom2.Click += new System.EventHandler(this.label2_Click_1);
            // 
            // label_id2
            // 
            this.label_id2.AutoSize = true;
            this.label_id2.Location = new System.Drawing.Point(64, 37);
            this.label_id2.Name = "label_id2";
            this.label_id2.Size = new System.Drawing.Size(20, 22);
            this.label_id2.TabIndex = 0;
            this.label_id2.Text = "id";
            // 
            // label_modifier
            // 
            this.label_modifier.AutoSize = true;
            this.label_modifier.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_modifier.Location = new System.Drawing.Point(424, 427);
            this.label_modifier.Name = "label_modifier";
            this.label_modifier.Size = new System.Drawing.Size(201, 22);
            this.label_modifier.TabIndex = 14;
            this.label_modifier.Text = "id de la ligne à modifier";
            this.label_modifier.Click += new System.EventHandler(this.label_modifier_Click);
            // 
            // textBox_modifier
            // 
            this.textBox_modifier.Location = new System.Drawing.Point(634, 427);
            this.textBox_modifier.Name = "textBox_modifier";
            this.textBox_modifier.Size = new System.Drawing.Size(100, 20);
            this.textBox_modifier.TabIndex = 15;
            this.textBox_modifier.TextChanged += new System.EventHandler(this.textBox_modifier_TextChanged);
            // 
            // quitter
            // 
            this.quitter.ForeColor = System.Drawing.Color.Red;
            this.quitter.Location = new System.Drawing.Point(788, 12);
            this.quitter.Name = "quitter";
            this.quitter.Size = new System.Drawing.Size(27, 23);
            this.quitter.TabIndex = 16;
            this.quitter.Text = "X";
            this.quitter.UseVisualStyleBackColor = true;
            this.quitter.Click += new System.EventHandler(this.quitter_Click);
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.Font = new System.Drawing.Font("Utsaah", 50F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.ForeColor = System.Drawing.Color.Linen;
            this.title.Location = new System.Drawing.Point(61, 245);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(614, 72);
            this.title.TabIndex = 17;
            this.title.Text = "Basic Task Manager 2019";
            this.title.Click += new System.EventHandler(this.title_Click);
            // 
            // btn_trier
            // 
            this.btn_trier.Location = new System.Drawing.Point(740, 322);
            this.btn_trier.Name = "btn_trier";
            this.btn_trier.Size = new System.Drawing.Size(75, 73);
            this.btn_trier.TabIndex = 18;
            this.btn_trier.Text = "Trier";
            this.btn_trier.UseVisualStyleBackColor = true;
            this.btn_trier.UseWaitCursor = true;
            this.btn_trier.Click += new System.EventHandler(this.btn_trier_Click);
            // 
            // col_nom
            // 
            this.col_nom.HeaderText = "nom";
            this.col_nom.Name = "col_nom";
            this.col_nom.ReadOnly = true;
            // 
            // col_id
            // 
            this.col_id.HeaderText = "id";
            this.col_id.Name = "col_id";
            this.col_id.ReadOnly = true;
            // 
            // col_intitule
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Lime;
            this.col_intitule.DefaultCellStyle = dataGridViewCellStyle2;
            this.col_intitule.HeaderText = "intitulé";
            this.col_intitule.Name = "col_intitule";
            this.col_intitule.ReadOnly = true;
            this.col_intitule.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.col_intitule.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // col_dead
            // 
            this.col_dead.HeaderText = "deadline";
            this.col_dead.Name = "col_dead";
            this.col_dead.ReadOnly = true;
            // 
            // BasicTaskManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.LightPink;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(827, 485);
            this.Controls.Add(this.btn_trier);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.title);
            this.Controls.Add(this.quitter);
            this.Controls.Add(this.textBox_modifier);
            this.Controls.Add(this.label_modifier);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBox_date);
            this.Controls.Add(this.textBox_int);
            this.Controls.Add(this.label_deadline);
            this.Controls.Add(this.label_int);
            this.Controls.Add(this.label_nom);
            this.Controls.Add(this.label_id);
            this.Controls.Add(this.textBox_nom);
            this.Controls.Add(this.textBox_id);
            this.Controls.Add(this.btn_supprimer);
            this.Controls.Add(this.btn_modifier);
            this.Controls.Add(this.btn_ajouter);
            this.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "BasicTaskManager";
            this.Text = "BasicTaskManager";
            this.TransparencyKey = System.Drawing.Color.MediumSeaGreen;
            this.Load += new System.EventHandler(this.BasicTaskManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_ajouter;
        private System.Windows.Forms.Button btn_modifier;
        private System.Windows.Forms.Button btn_supprimer;
        private System.Windows.Forms.TextBox textBox_id;
        private System.Windows.Forms.TextBox textBox_nom;
        private System.Windows.Forms.Label label_id;
        private System.Windows.Forms.Label label_nom;
        private System.Windows.Forms.Label label_int;
        private System.Windows.Forms.Label label_deadline;
        private System.Windows.Forms.TextBox textBox_int;
        private System.Windows.Forms.TextBox textBox_date;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox_nom2;
        private System.Windows.Forms.TextBox textBox_id2;
        private System.Windows.Forms.Label label_deadline2;
        private System.Windows.Forms.Label label_int2;
        private System.Windows.Forms.Label label_nom2;
        private System.Windows.Forms.Label label_id2;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.TextBox textBox_deadline2;
        private System.Windows.Forms.TextBox textBox_int2;
        private System.Windows.Forms.Label label_modifier;
        private System.Windows.Forms.TextBox textBox_modifier;
        private System.Windows.Forms.Button quitter;
        private System.Windows.Forms.Label title;
        private System.Windows.Forms.Button btn_trier;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_nom;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_intitule;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_dead;
    }
}

